package com.generator;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.LikeTable;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;

import java.util.Collections;

/**
 * @Author: youqi.huang
 * @Description:
 * @CreateTime: 2021/10/30 19:12
 */
public class MybatisPlusGenerator {
    public static void main(String[] args) {
        FastAutoGenerator.create(
                "jdbc:mysql://127.0.0.1:3306/miai?useSSL=false&serverTimezone=Asia/Shanghai&characterEncoding=utf-8&autoReconnect=true",
                        "root",
                        "root")
                .globalConfig(builder -> {
                    builder
                            .disableOpenDir()
                            .author("youqi.huang") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .enableSwagger() // 开启swagger
                            .dateType(DateType.TIME_PACK)
                            .outputDir("./src/main/java/"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder
                            .entity("entity") // Entity 包名 默认值:entity
                            .parent("com.generator") // 设置父包名
                            .moduleName("system") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "./src/main/resources/mapper/")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    // 设置需要生成的表名
                    builder.addInclude("member_base")
                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .strategyConfig(builder -> {
                    builder.entityBuilder()
                            // 添加表字段填充
                            .addTableFills(new Column("create_time", FieldFill.INSERT))
                            // 添加表字段填充
                            .addTableFills(new Property("update_time", FieldFill.INSERT_UPDATE))
                            // 设置id策略微雪花id
                            .idType(IdType.ASSIGN_ID);
                })
                .templateEngine(new VelocityTemplateEngine()) // 使用Velocity引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
